<?php

$classesDir = array (
    __DIR__ . '/../app/Framework/Controllers',
    __DIR__ . '/../app/Framework/Models',
    __DIR__ . '/../app/Controllers',
    __DIR__ . '/../settings',
    __DIR__ . '/../web'
);

foreach ($classesDir as $key => $dir) {
    foreach (glob($dir . "/*.php") as $filename) {
        include $filename;
    }
}

use app\Framework\Controllers\Redirect;

$app = new Redirect($web);
$app->run();