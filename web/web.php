<?php

$web = [
    '/user/index' => ['UserController', 'index'],
    '/user/edit' => ['UserController', 'edit'],
    '/user/create' => ['UserController', 'create'],
];