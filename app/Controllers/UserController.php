<?php

namespace app\Controllers;

use app\Framework\Models\View;

class UserController
{

    private $view;

    public function __construct($view)
    {
        $this->view = $view;
    }

    public function index()
    {
        $view = new View();
        $view->render($this->view);
    }
}