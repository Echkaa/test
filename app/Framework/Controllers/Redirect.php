<?php

namespace app\Framework\Controllers;

class Redirect
{

    private $web = [];          //Web url in array data
    private $page = '';

    public function __construct($web)
    {
        $this->web = $web;
        $this->page = $_SERVER['REQUEST_URI'];
    }

    public function run()
    {
        if (array_key_exists($this->page, $this->web)) {
            $controller = 'app\Controllers\\' . $this->web[$this->page][0];

            $controller = new $controller($this->web[$this->page][1]);
            $controller->index();

        } else {
            echo 'Page not found';
        }
    }
}